//
//  HistoryDetailVC.m
//  Rider Driver
//
//  Created by My Mac on 7/8/15.
//  Copyright (c) 2015 Elluminati. All rights reserved.
//

#import "HistoryDetailVC.h"
#import "AFNHelper.h"
#import "UIImageView+Download.h"

@interface HistoryDetailVC ()

@end

@implementation HistoryDetailVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"LOADING_HISTORY", nil)];
    self.navigationItem.hidesBackButton = YES;
       // [self downloadCover];
    [self setTripDetails];
    [self.btnBack setTitle:NSLocalizedString(@"History", nil) forState:UIControlStateNormal];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setTripDetails
{
    [lblCost setText:[NSString stringWithFormat:@"%@ %.2f",[self.dictInfo valueForKey:@"currency"],[[self.dictInfo objectForKey:@"total"] floatValue]]];
    [lblMinutes setText:[NSString stringWithFormat:@"%.2f mins",[[self.dictInfo objectForKey:@"time"] floatValue]]];
    [lblDistance setText:[NSString stringWithFormat:@"%.2f kms",[[self.dictInfo objectForKey:@"distance"] floatValue]]];
    
    [txtSource setText:[NSString stringWithFormat:@"%@",[self.dictInfo objectForKey:@"src_address"]]];
    [txtDestination setText:[NSString stringWithFormat:@"%@",[self.dictInfo objectForKey:@"dest_address"]]];
    
    [mapImgView downloadFromURL:[self.dictInfo objectForKey:@"map_url"] withPlaceholder:[UIImage imageNamed:@"no_items_display"]];
    [APPDELEGATE hideLoadingView];
}

- (IBAction)btnBackPressed:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end